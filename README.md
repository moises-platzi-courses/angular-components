# Angular Components

Divide tu proyecto en componentes y conoce más sobre la arquitectura de este framework. Conecta tus aplicaciones con una API para obtener datos de un sistema de Backend. Y ¡Sigue potenciando tu tienda en línea!

+ Conocer sobre los componentes y su interacción
+ Obtener datos de una API
+ Practicar y entender sobre programación reactiva

## Contenido del Curso

### Componentes
+ ¿Qué son los componentes?
+ Uso de Inputs
+ Uso de Outputs
+ Componente para producto
+ Ciclo de vida de compoentes
+ ngDestroy & SetInput
+ Lista de productos
+ Componentes y Header
+ Implementando el sideMenu
+ Comunicación padre e hijo

### Servicios
+ Conociendo los servicios
+ ¿Qué es la inyección de dependencias?
+ Obteniendo datos de una API

### Pipes y Directives
+ Conociendo los pipes
+ Construyendo tu propio pipe
+ Conociendo las directivas

### Best practices
+ Reactividad básica
+ Guía de estilos de Angular y linters
